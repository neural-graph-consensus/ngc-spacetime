import numpy as np
import matplotlib.pyplot as plt
import flow_vis
from typing import Iterator
from neural_wrappers.graph import Graph
from neural_wrappers.utilities import npGetInfo
from pathlib import Path
from media_processing_lib.image import tryWriteImage
from matplotlib.cm import hot
from copy import deepcopy

from models.warp_flow import npWarpFlow
from models.warp_delta_pose import npWarpDepthPose, computeIntrinsics
from cycleconcepts.nodes import *

cnt = 0

def toImage(x):
	x = minMax(x)
	x = x if x.shape[-1] != 1 else x[..., 0]
	x = (np.clip(x, 0, 1) * 255).astype(np.uint8)
	return x

def minMax(x, low=0, high=100):
	Min, Max = np.percentile(x, [low, high])
	x = np.clip(x, Min, Max)
	return (x - x.min()) / (x.max() - x.min() + np.spacing(1))

def Hot(x):
	return hot(minMax(x))[..., 0 : 3]

def plotFlowPrev(data):
	data = deepcopy(data)
	rgb = data["rgb"]
	rgbPrev = data["rgb(t-1)"]
	flowPrev = data["optical_flow(t-1, t)"]
	rgbWarpPrev = npWarpFlow(rgbPrev.transpose(0, 3, 1, 2), flowPrev).transpose(0, 2, 3, 1)
	diffRGBWarpPrev = Hot(np.abs(rgb - rgbWarpPrev).sum(-1))

	items = [toImage(item) for item in [rgb, rgbPrev, rgbWarpPrev, diffRGBWarpPrev]]
	rgb, rgbPrev, rgbWarpPrev, diffRGBWarpPrev = items
	flowPrev = np.array([flow_vis.flow_to_color((minMax(flow, 5, 95) - 0.5) * 2) for flow in flowPrev])
	stack = np.concatenate([rgb, rgbPrev, flowPrev, rgbWarpPrev, diffRGBWarpPrev], axis=2)
	return stack

def plotFlowNext(data):
	data = deepcopy(data)
	rgb = data["rgb"]
	rgbNext = data["rgb(t+1)"]
	flowNext = data["optical_flow(t+1, t)"]
	rgbWarpNext = npWarpFlow(rgbNext.transpose(0, 3, 1, 2), flowNext).transpose(0, 2, 3, 1)
	diffRGBWarpNext = Hot(np.abs(rgb - rgbWarpNext).sum(-1))

	items = [toImage(item) for item in [rgb, rgbNext, rgbWarpNext, diffRGBWarpNext]]
	rgb, rgbNext, rgbWarpNext, diffRGBWarpNext = items
	flowNext = np.array([flow_vis.flow_to_color((minMax(flow, 5, 95) - 0.5) * 2) for flow in flowNext])
	stack = np.concatenate([rgb, rgbNext, flowNext, rgbWarpNext, diffRGBWarpNext], axis=2)
	return stack

def plotDepthPrev(data):
	data = deepcopy(data)
	rgb = data["rgb"]
	rgbPrev = data["rgb(t-1)"]
	depth = data["depth"][..., 0]
	posePrev = data["pose(t-1)"]
	pose = data["pose"]
	K = computeIntrinsics(resolution=(rgb.shape[1], rgb.shape[2]))
	rgbWarpPrev = npWarpDepthPose(rgbPrev.transpose(0, 3, 1, 2), depth, posePrev, pose, K).transpose(0, 2, 3, 1)
	diffRGBWarpPrev = Hot(np.abs(rgb - rgbWarpPrev).sum(-1))

	depth = Hot(depth)
	items = [toImage(item) for item in [rgb, rgbPrev, rgbWarpPrev, diffRGBWarpPrev, depth]]
	rgb, rgbPrev, rgbWarpPrev, diffRGBWarpPrev, depth = items
	stack = np.concatenate([rgb, rgbPrev, depth, rgbWarpPrev, diffRGBWarpPrev], axis=2)
	return stack

def plotDepthNext(data):
	data = deepcopy(data)
	rgb = data["rgb"]
	rgbNext = data["rgb(t+1)"]
	depth = data["depth"][..., 0]
	poseNext = data["pose(t+1)"]
	pose = data["pose"]
	K = computeIntrinsics(resolution=(rgb.shape[1], rgb.shape[2]))
	rgbWarpNext = npWarpDepthPose(rgbNext.transpose(0, 3, 1, 2), depth, poseNext, pose, K).transpose(0, 2, 3, 1)
	diffRGBWarpNext = Hot(np.abs(rgb - rgbWarpNext).sum(-1))

	depth = Hot(depth)
	items = [toImage(item) for item in [rgb, rgbNext, rgbWarpNext, diffRGBWarpNext, depth]]
	rgb, rgbNext, rgbWarpNext, diffRGBWarpNext, depth = items
	stack = np.concatenate([rgb, rgbNext, depth, rgbWarpNext, diffRGBWarpNext], axis=2)
	return stack

def warp_plot_dataset(generator:Iterator, numSteps:int):
	baseDir = "warp_plot_dataset"
	Path(baseDir).mkdir(exist_ok=True)
	global cnt

	for i in range(numSteps):
		data, labels = next(generator)[0]
		print("Data:")
		for k in sorted(data.keys()):
			print(" - %s: %s" % (k, npGetInfo(data[k])))

		flowPrevStack = plotFlowPrev(data)
		flowNextStack = plotFlowNext(data)
		depthPrevStack = plotDepthPrev(data)
		depthNextStack = plotDepthNext(data)
		stack = np.concatenate([flowPrevStack, flowNextStack, depthPrevStack, depthNextStack], axis=1)

		for j in range(len(stack)):
			cnt += 1
			tryWriteImage(stack[j], "%s/%d_stack.png" % (baseDir, cnt))
			# plt.imshow(stack[j])
			# plt.axis("off")
			# plt.show()

			if cnt > 20:
				exit()