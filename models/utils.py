from neural_wrappers.graph import MapNode
from overrides import overrides

class DeltaNode(MapNode):
	def __init__(self, node, delta):
		assert delta in (-1, 1)
		self.node = node
		self.delta = delta
		super(MapNode, self).__init__(name="%s (t%+d)" % (node.name, delta), \
			groundTruthKey="%s(t%+d)" % (node.groundTruthKey, delta))

	@overrides
	def getEncoder(self, outputNodeType=None):
		return self.node.getEncoder(outputNodeType)

	@overrides
	def getDecoder(self, inputNodeType=None):
		return self.node.getDecoder(inputNodeType)

	@overrides
	def getMetrics(self):
		return self.node.getMetrics()

	@overrides
	def getCriterion(self):
		return self.node.getCriterion()

class PrevNode(DeltaNode):
	def __init__(self, node):
		super().__init__(node, delta=-1)

class NextNode(DeltaNode):
	def __init__(self, node):
		super().__init__(node, delta=+1)
