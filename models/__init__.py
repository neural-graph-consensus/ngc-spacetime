# CFGs from NGC paper
import os
import sys
cwd = os.path.abspath(os.path.realpath(os.path.dirname(__file__)))
sys.path.append("%s/../../neural-graph-consensus" % cwd)
from cycleconcepts.cfgs import rgb2wireframeregression_graph_ensemble, rgb2depth_graph_ensemble, \
    rgb2normal_graph_ensemble, rgb2cameranormal_graph_ensemble, rgb2semantic_graph_ensemble, \
    rgb2wireframeregression, rgb2depth, rgb2normal, rgb2cameranormal, \
    rgb2semantic, rgb2halftone
from cycleconcepts.models.GraphTypes import SingleLinkGraph, GraphEnsemble

# CFGs using warping
from .GraphTypes import WarpFlowGraph, WarpDepthPoseGraph
from .depth_cfgs import *
from .flow_cfgs import *
