import torch as tr
from neural_wrappers.graph import Graph, Edge, forwardUseGT, forwardUseIntermediateResult, ReduceNode, MapNode

from cycleconcepts.utils import getModelDataDimsAndHyperParams, fullPath, addNodesArguments
from cycleconcepts.nodes import *
from cycleconcepts.models import *
from cycleconcepts.cfgs.graph_ensemble.utils import meanFunction
from ..warp_delta_pose import warpFunction

class RGBPPrev(RGB):
	def __init__(self):
		super(MapNode, self).__init__(name="RGB (t-1)", groundTruthKey="rgb(t-1)")

class WireframeWarpNode(WireframeRegression):
	def __init__(self):
		super(MapNode, self).__init__(name="Wireframe Regression Warp (t-1)", \
			groundTruthKey=["wireframe_regression", "depth(t-1)", "pose(t-1)", "pose", \
				"wireframe_regression(t-1)", "rgb", "rgb(t-1)"])

	def getCriterion(self):
		return lambda y, t : None

	def getMetrics(self):
		return {}

def rgb2wireframeregression_warp_depth(singleLinksPath:str) -> GraphEnsemble:
	# Graph time T
	rgbNode = RGB()
	wireframeNode = WireframeRegression()
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True)
	# rgb2wireframe.loadWeights("%s/rgb2wireframe/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	# Graph time T-1
	rgbPrevNode = RGBPPrev()
	wireframeWarpNode = WireframeWarpNode()
	rgbPrev2wireframe = Edge(rgbPrevNode, wireframeWarpNode, forwardFn=forwardUseGT, blockGradients=True)
	wireframeWarp = ReduceNode(wireframeWarpNode, forwardFn=warpFunction)
	# rgbPrev2wireframe.loadWeights("%s/rgb2wireframe/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	wireframePrevToCurrent = Edge(wireframeWarpNode, wireframeNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	wireframeMean = ReduceNode(wireframeNode, forwardFn=meanFunction)

	graph = Graph([
		# Graph time T
		rgb2wireframe,
		# Graph time T-1
		rgbPrev2wireframe,
		wireframeWarp,
		# Combining them
		wireframePrevToCurrent,
		wireframeMean
	])

	return graph