from neural_wrappers.graph import Graph

class WarpFlowGraph(Graph): pass
class WarpDepthPoseGraph(Graph): pass
class WarpFlowGraphEnsemble(Graph): pass
