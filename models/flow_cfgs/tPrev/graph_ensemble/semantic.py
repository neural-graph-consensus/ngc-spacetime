import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph import Graph, Edge, forwardUseGT, ReduceNode, MapNode, forwardUseIntermediateResult

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraphEnsemble
from ....utils import PrevNode

def rgb2semantic_graph_ensemble_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[Graph, tr.Tensor], tr.Tensor]) -> WarpFlowGraphEnsemble:
	singleLinksPath = hyperParameters["singleLinksPath"]

	rgbNode = RGB()
	halftoneNode = Halftone()
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgbPrev = PrevNode(rgbNode)
	halftonePrev = PrevNode(halftoneNode)
	normalPrev = PrevNode(normalNode)
	semanticPrev = PrevNode(semanticNode)

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->semantic(t)")
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->halftone(t)")
	halftone2semantic = Edge(halftoneNode, semanticNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="halftone(t)->semantic(t)")
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->normal(t)")
	normal2semantic = Edge(normalNode, semanticNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="normal(t)->semantic(t)")

	rgb2semanticPrev = Edge(rgbPrev, semanticPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->semantic(t-1)")
	rgb2halftonePrev = Edge(rgbPrev, halftonePrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->halftone(t-1)")
	halftone2semanticPrev = Edge(halftonePrev, semanticPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="halftone(t-1)->semantic(t-1)")
	rgb2normalPrev = Edge(rgbPrev, normalPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->normal(t-1)")
	normal2semanticPrev = Edge(normalPrev, semanticPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="normal(t-1)->semantic(t-1)")

	semanticPrev2Semantic = Edge(semanticPrev, semanticNode, forwardFn=warpFunction, useLoss=False, useMetrics=False, \
		blockGradients=True, name="Warp(t-1, t)")
	voteEdge = ReduceNode(semanticNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	edges = [rgb2semantic, rgb2halftone, halftone2semantic, rgb2normal, normal2semantic]
	edgesPrev = [rgb2semanticPrev, rgb2halftonePrev, halftone2semanticPrev, rgb2normalPrev, normal2semanticPrev]
	edgeNames = ["rgb2semantic", "rgb2halftone", "halftone2semantic", "rgb2normal", "normal2semantic"]

	for edge, edgePrev, name in zip(edges, edgesPrev, edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
		edgePrev.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)

	graph = WarpFlowGraphEnsemble([
		*edges,
		*edgesPrev,

		semanticPrev2Semantic,
		voteEdge
	])

	return graph