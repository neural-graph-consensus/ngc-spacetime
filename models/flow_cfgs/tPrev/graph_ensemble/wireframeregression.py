import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph import Graph, Edge, forwardUseGT, ReduceNode, MapNode, forwardUseIntermediateResult

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraphEnsemble
from ....utils import PrevNode

def rgb2wireframeregression_graph_ensemble_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[Graph, tr.Tensor], tr.Tensor]) -> WarpFlowGraphEnsemble:
	singleLinksPath = hyperParameters["singleLinksPath"]

	rgbNode = RGB()
	halftoneNode = Halftone()
	wireframeNode = WireframeRegression()

	rgbPrev = PrevNode(rgbNode)
	halftonePrev = PrevNode(halftoneNode)
	wireframePrev = PrevNode(wireframeNode)

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->halftone(t)")
	halftone2wireframe = Edge(halftoneNode, wireframeNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="halftone(t)->wireframe(t)")
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->wireframe(t)")

	rgb2halftonePrev = Edge(rgbPrev, halftonePrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->halftone(t-1)")
	halftone2wireframePrev = Edge(halftonePrev, wireframePrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="halftone(t-1)->wireframe(t-1)")
	rgb2wireframePrev = Edge(rgbPrev, wireframePrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->wireframe(t-1)")

	wireframePrev2wireframe = Edge(wireframePrev, wireframeNode, forwardFn=warpFunction, useLoss=False, useMetrics=False, \
		blockGradients=True, name="Warp(t-1, t)")
	voteEdge = ReduceNode(wireframeNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	edges = [rgb2wireframe, rgb2halftone, halftone2wireframe]
	edgesPrev = [rgb2wireframePrev, rgb2halftonePrev, halftone2wireframePrev]
	edgeNames = ["rgb2wireframe", "rgb2halftone", "halftone2wireframe"]
	for edge, edgePrev, name in zip(edges, edgesPrev, edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
		edgePrev.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)

	graph = WarpFlowGraphEnsemble([
		*edges,
		*edgesPrev,

		wireframePrev2wireframe,
		voteEdge
	])

	return graph
