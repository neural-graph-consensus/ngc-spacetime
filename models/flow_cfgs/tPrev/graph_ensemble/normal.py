import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph import Graph, Edge, forwardUseGT, ReduceNode, MapNode, forwardUseIntermediateResult

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraphEnsemble
from ....utils import PrevNode

def rgb2normal_graph_ensemble_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[Graph, tr.Tensor], tr.Tensor]) -> WarpFlowGraphEnsemble:
	singleLinksPath = hyperParameters["singleLinksPath"]

	rgbNode = RGB()
	normalNode = Normal()
	wireframeNode = WireframeRegression()
	cameraNormalNode = CameraNormal()
	halftoneNode = Halftone()

	rgbPrev = PrevNode(rgbNode)
	normalPrev = PrevNode(normalNode)
	wireframePrev = PrevNode(wireframeNode)
	cameraNormalPrev = PrevNode(cameraNormalNode)
	halftonePrev = PrevNode(halftoneNode)

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True, name="RGB(t) -> Normal(t)")
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True, \
		name = "RGB(t) -> Wireframe(t)")
	wireframe2normal = Edge(wireframeNode, normalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="Wireframe(t) -> Normal(t)")
	rgb2cameraNormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True, \
		name="RGB(t) -> CameraNormal(t)")
	cameraNormal2normal = Edge(cameraNormalNode, normalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="CameraNormal(t) -> Normal(t)")
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True, \
		name="RGB(t) -> Halftone(t)")
	halftone2normal = Edge(halftoneNode, normalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="Halftone(t) -> Normal(t)")

	rgb2normalPrev = Edge(rgbPrev, normalPrev, forwardFn=forwardUseGT, blockGradients=True, \
		name="RGB(t-1) -> Normal(t-1)")
	rgb2wireframePrev = Edge(rgbPrev, wireframePrev, forwardFn=forwardUseGT, blockGradients=True, \
		name = "RGB(t-1) -> Wireframe(t-1)")
	wireframe2normalPrev = Edge(wireframePrev, normalPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="Wireframe(t-1) -> Normal(t-1)")
	rgb2cameraNormalPrev = Edge(rgbPrev, cameraNormalPrev, forwardFn=forwardUseGT, blockGradients=True, \
		name="RGB(t-1) -> CameraNormal(t-1)")
	cameraNormal2normalPrev = Edge(cameraNormalPrev, normalPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="CameraNormal(t-1) -> Normal(t-1)")
	rgb2halftonePrev = Edge(rgbPrev, halftonePrev, forwardFn=forwardUseGT, blockGradients=True, \
		name="RGB(t-1) -> Halftone(t-1)")
	halftone2normalPrev = Edge(halftonePrev, normalPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="Halftone(t-1) -> Normal(t-1)")

	normalPrev2normal = Edge(normalPrev, normalNode, forwardFn=warpFunction, useLoss=False, useMetrics=False, \
		blockGradients=True, name="Warp(t-1, t)")
	voteEdge = ReduceNode(normalNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	Strs = ["rgb2normal", "rgb2wireframe", "wireframe2normal", "rgb2cameranormal", "cameranormal2normal", \
		"rgb2halftone", "halftone2normal", "rgb2normal", "rgb2wireframe", "wireframe2normal", \
		"rgb2cameranormal", "cameranormal2normal", "rgb2halftone", "halftone2normal"]
	edges = [rgb2normal, rgb2wireframe, wireframe2normal, rgb2cameraNormal, cameraNormal2normal, \
		rgb2halftone, halftone2normal, rgb2normalPrev, rgb2wireframePrev, wireframe2normalPrev, \
		rgb2cameraNormalPrev, cameraNormal2normalPrev, rgb2halftonePrev, halftone2normalPrev]

	for Str, edge in zip(Strs, edges):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, Str), yolo=True)

	graph = WarpFlowGraphEnsemble([
		*edges,

		normalPrev2normal,
		voteEdge
	])

	return graph
