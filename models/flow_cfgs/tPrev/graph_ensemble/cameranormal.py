import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph import Graph, Edge, forwardUseGT, ReduceNode, MapNode, forwardUseIntermediateResult

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraphEnsemble
from ....utils import PrevNode

def rgb2cameranormal_graph_ensemble_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[Graph, tr.Tensor], tr.Tensor]) -> WarpFlowGraphEnsemble:
	singleLinksPath = hyperParameters["singleLinksPath"]

	rgbNode = RGB()
	cameraNormalNode = CameraNormal()
	normalNode = Normal()
	wireframeNode = WireframeRegression()

	rgbPrev = PrevNode(rgbNode)
	cameraNormalPrev = PrevNode(cameraNormalNode)
	normalPrev = PrevNode(normalNode)
	wireframePrev = PrevNode(wireframeNode)

	rgb2cameraNormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->cameranormal(t)")
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->normal(t)")
	normal2cameraNormal = Edge(normalNode, cameraNormalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="normal(t)->cameranormal(t)")
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->wireframeregression(t)")
	wireframe2cameraNormal = Edge(wireframeNode, cameraNormalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="wireframeregression(t)->cameranormal(t)")

	rgb2cameraNormalPrev = Edge(rgbPrev, cameraNormalPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->cameranormal(t-1)")
	rgb2normalPrev = Edge(rgbPrev, normalPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->normal(t-1)")
	normal2cameraNormalPrev = Edge(normalPrev, cameraNormalPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="normal(t-1)->cameranormal(t-1)")
	rgb2wireframePrev = Edge(rgbPrev, wireframePrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->wireframeregression(t-1)")
	wireframe2cameraNormalPrev = Edge(wireframePrev, cameraNormalPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="wireframeregression(t-1)->cameranormal(t-1)")

	cameraNormalPrev2cameraNormal = Edge(cameraNormalPrev, cameraNormalNode, forwardFn=warpFunction, useLoss=False, useMetrics=False, \
		blockGradients=True, name="Warp(t-1, t)")
	voteEdge = ReduceNode(cameraNormalNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	edges = [rgb2cameraNormal, rgb2normal, normal2cameraNormal, rgb2wireframe, wireframe2cameraNormal]
	edgesPrev = [rgb2cameraNormalPrev, rgb2normalPrev, normal2cameraNormalPrev, rgb2wireframePrev, wireframe2cameraNormalPrev]
	edgeNames = ["rgb2cameranormal", "rgb2normal", "normal2cameranormal", "rgb2wireframe", "wireframe2cameranormal"]
	for edge, edgePrev, name in zip(edges, edgesPrev, edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
		edgePrev.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)

	graph = WarpFlowGraphEnsemble([
		*edges,
		*edgesPrev,

		cameraNormalPrev2cameraNormal,
		voteEdge
	])
	return graph