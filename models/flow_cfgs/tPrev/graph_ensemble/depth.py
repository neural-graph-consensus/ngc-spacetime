import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph import Graph, Edge, forwardUseGT, ReduceNode, MapNode, forwardUseIntermediateResult

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraphEnsemble
from ....utils import PrevNode

def rgb2depth_graph_ensemble_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[Graph, tr.Tensor], tr.Tensor]) -> WarpFlowGraphEnsemble:
	singleLinksPath = hyperParameters["singleLinksPath"]

	rgbNode = RGB()
	halftoneNode = Halftone()
	cameraNormalNode = CameraNormal()
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgbPrev = PrevNode(rgbNode)
	halftonePrev = PrevNode(halftoneNode)
	cameraNormalPrev = PrevNode(cameraNormalNode)
	normalPrev = PrevNode(normalNode)
	semanticPrev = PrevNode(semanticNode)
	depthPrev = PrevNode(depthNode)

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->depth(t)")
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->halftone(t)")
	halftone2depth = Edge(halftoneNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="halftone(t)->depth(t)")
	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->semantic(t)")
	semantic2depth = Edge(semanticNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="semantic(t)->depth(t)")
	rgb2cameranormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->cameranormal(t)")
	cameranormal2depth = Edge(cameraNormalNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="cameranormal(t)->depth(t)")
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t)->normal(t)")
	normal2depth = Edge(normalNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="normal(t)->depth(t)")

	rgb2depthPrev = Edge(rgbPrev, depthPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->depth(t-1)")
	rgb2halftonePrev = Edge(rgbPrev, halftonePrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->halftone(t-1)")
	halftone2depthPrev = Edge(halftonePrev, depthPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="halftone(t-1)->depth(t-1)")
	rgb2semanticPrev = Edge(rgbPrev, semanticPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->semantic(t-1)")
	semantic2depthPrev = Edge(semanticPrev, depthPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="semantic(t-1)->depth(t-1)")
	rgb2cameranormalPrev = Edge(rgbPrev, cameraNormalPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->cameranormal(t-1)")
	cameranormal2depthPrev = Edge(cameraNormalPrev, depthPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="cameranormal(t-1)->depth(t-1)")
	rgb2normalPrev = Edge(rgbPrev, normalPrev, forwardFn=forwardUseGT, blockGradients=True, name="rgb(t-1)->normal(t-1)")
	normal2depthPrev = Edge(normalPrev, depthPrev, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True, name="normal(t-1)->depth(t-1)")

	depthPrev2Depth = Edge(depthPrev, depthNode, forwardFn=warpFunction, useLoss=False, useMetrics=False, \
		blockGradients=True, name="Warp(t-1, t)")
	voteEdge = ReduceNode(depthNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	edges = [rgb2depth, rgb2halftone, halftone2depth, rgb2semantic, semantic2depth, rgb2cameranormal, cameranormal2depth, rgb2normal, normal2depth]
	edgesPrev = [rgb2depthPrev, rgb2halftonePrev, halftone2depthPrev, rgb2semanticPrev, semantic2depthPrev, rgb2cameranormalPrev, cameranormal2depthPrev, rgb2normalPrev, normal2depthPrev]
	Strs = ["rgb2depth", "rgb2halftone", "halftone2depth", "rgb2semantic", "semantic2depth", "rgb2cameranormal", "cameranormal2depth", "rgb2normal", "normal2depth"]

	for Str, edge, edgePrev in zip(Strs, edges, edgesPrev):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, Str), yolo=True)
		edgePrev.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, Str), yolo=True)

	graph = WarpFlowGraphEnsemble([
		*edges,
		*edgesPrev,

		depthPrev2Depth,
		voteEdge
	])

	return graph
