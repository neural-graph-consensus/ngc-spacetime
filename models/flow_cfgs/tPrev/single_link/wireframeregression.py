import torch as tr
from typing import Dict, Any, Optional, Callable
from neural_wrappers.graph import Edge, forwardUseGT, ReduceNode, MapNode

from cycleconcepts.nodes import *
from ....warp_flow import warpFunction
from ....GraphTypes import WarpFlowGraph
from ....utils import PrevNode

def rgb2wireframeregression_warp_flow(hyperParameters:Dict[str, Any], \
	voteFunction:Callable[[WarpFlowGraph, tr.Tensor], tr.Tensor]) -> WarpFlowGraph:
	singleLinksPath = hyperParameters["singleLinksPath"]
	# Nodes: RGB, RGB(t-1) and WireframeRegression
	rgbNode = RGB()
	rgbPrevNode = PrevNode(rgbNode)
	wireframeregressionNode = WireframeRegression()
	wireframeregressionPrev = PrevNode(wireframeregressionNode)
	# Edges: rgb(t)->wireframeregression(t) & rgb(t-1)->wireframeregression(t-1)
	rgb2wireframeregression = Edge(rgbNode, wireframeregressionNode, forwardFn=forwardUseGT, blockGradients=True, \
		name="Single Link(t)")
	rgbPrev2wireframeregression = Edge(rgbPrevNode, wireframeregressionPrev, forwardFn=forwardUseGT, \
		blockGradients=True, name="Single Link(t-1)")
	# Edge: wireframeregression(t-1) + optical_flow(t-1, t) -> wireframeregression(t)
	wireframeregressionPrev2WireframeRegression = Edge(wireframeregressionPrev, wireframeregressionNode, forwardFn=warpFunction, \
		blockGradients=True, name="Warp(t-1, t)", useLoss=False, useMetrics=False)
	# Edge: Vote([wireframeregression(t)]) -> wireframeregression(t)
	voteEdge = ReduceNode(wireframeregressionNode, forwardFn=voteFunction, name="Vote", useMetrics=True, useLoss=True)

	# Load Single links
	rgb2wireframeregression.loadWeights("%s/rgb2wireframe/model_best_Loss.pkl" % singleLinksPath, yolo=True)
	rgbPrev2wireframeregression.loadWeights("%s/rgb2wireframe/model_best_Loss.pkl" % singleLinksPath, yolo=True)

	graph = WarpFlowGraph([
		rgb2wireframeregression,
		rgbPrev2wireframeregression,
		wireframeregressionPrev2WireframeRegression,
		voteEdge
	])

	return graph