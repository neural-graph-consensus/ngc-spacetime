import numpy as np
import torch as tr
import h5py
from argparse import ArgumentParser
from functools import partial
from typing import get_type_hints

from neural_wrappers.utilities import changeDirectory, getGenerators, npGetInfo
from neural_wrappers.pytorch import device
from neural_wrappers.readers import CarlaH5PathsReader, StaticBatchedDatasetReader
from media_processing_lib.image import tryWriteImage

from models import *
from cycleconcepts.voting_algorithms import *
from voting_algorithms import *
# from reader import Reader
from warp_plot import warp_plot
from warp_plot_dataset import warp_plot_dataset
# from warp_benchmark import warp_benchmark

tr.backends.cudnn.deterministic = True
tr.backends.cudnn.benchmark = False
np.set_printoptions(precision=6, suppress=True)

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("datasetPath")
	parser.add_argument("--modelType")
	parser.add_argument("--singleLinksPath", help="Path to load weights for Single Links from")

	# Models
	parser.add_argument("--batchSize", default=5, type=int)

	# Voting stuff
	parser.add_argument("--votingAlgorithm")
	parser.add_argument("--maskedMeanWeight", type=str)

	args = parser.parse_args()
	assert args.type in ("warp_benchmark", "warp_plot", "test", "warp_plot_dataset")

	return args

def getModel(modelType, hyperParameters):
	model = globals()[modelType]

	returnType = get_type_hints(model)["return"]
	if returnType == GraphEnsemble:
		model = model(hyperParameters, singleLinksPath=hyperParameters["singleLinksPath"])
	elif returnType == SingleLinkGraph:
		model = model(hyperParameters)
		if modelType == "wireframeregression":
			weightsPath = "%s/wireframe/model_best_Loss.pkl" % hyperParameters["singleLinksPath"]
		else:
			weightsPath = "%s/%s/model_best_Loss.pkl" % (hyperParameters["singleLinksPath"], modelType)
		model.loadWeights(weightsPath, yolo=True)
	else:
		model = model(hyperParameters, voteFunction=hyperParameters["voteFunction"])

	return model

def getVotingAlgorithm(args):
	if args.votingAlgorithm == "simple_mean":
		return simpleMean
	elif args.votingAlgorithm == "simple_median":
		return simpleMedian
	elif args.votingAlgorithm == "masked_mean":
		return maskedMean
	elif args.votingAlgorithm == "masked_median":
		return maskedMedian
	elif args.votingAlgorithm == "masked_mean_weighted":
		weight = list(map(lambda x : float(x), args.maskedMeanWeight.split(",")))
		return partial(maskedMeanWeighted, weight=weight)
	else:
		assert False

def main():
	args = getArgs()

	if args.type == "warp_benchmark":
		warp_benchmark(args.datasetPath, args.batchSize)
		exit()

	# Just add everything to the reader :)
	Keys = ["rgb", "depth", "wireframe_regression", "pose", "semantic_segmentation", \
		"normal", "cameranormal", "halftone", "optical_flow"]
	# Default hyperparameters used in original paper.
	hyperParameters={
		"resolution" : (256, 256), "maxDepthMeters" : 300.0, "positionNormalization" : "none", \
		"orientationRepresentation" : "euler", "orientationNormalization" : "none", \
		"opticalFlowMode" : "xy", "opticalFlowPercentage" : (100, 100), "semanticNumClasses" : 13
	}
	deltas = [-1, +1]

	reader = StaticBatchedDatasetReader(
		CarlaH5PathsReader(h5py.File(args.datasetPath, "r")["train"], dataBuckets={"data" : Keys},
			deltas=deltas, hyperParameters=hyperParameters),
		args.batchSize)
	generator = reader.iterateOneEpoch()
	print(reader)

	if args.type == "warp_plot_dataset":
		warp_plot_dataset(generator, len(generator))

	print("Voting algorithm: %s" % args.votingAlgorithm)
	print("Single links path: %s" % args.singleLinksPath)
	hyperParameters["voteFunction"] = getVotingAlgorithm(args)
	hyperParameters["singleLinksPath"] = args.singleLinksPath
	model = getModel(args.modelType, hyperParameters).to(device)
	print(model.summary())

	if args.type == "test":
		results = model.test_generator(generator, len(generator), printMessage="v2")["Test"]
		print("\nResults:")
		for k in results:
			print(" - %s : %s" % (k, str(results[k])))
	elif args.type == "warp_plot":
		warp_plot(model, generator, len(generator))

if __name__ == "__main__":
	main()
