import torch as tr
import numpy as np
from neural_wrappers.pytorch import device

def maskedMean(self, x):
	x.pop("GT")
	flow = self.fullGT["optical_flow(t-1, t)"]

	MB, h, w = flow.shape[0], flow.shape[1], flow.shape[2]
	flow = (flow - 0.5) * 2
	flow_neutral = np.stack(np.meshgrid(np.linspace(-1, 1, w), np.linspace(-1, 1, h)), axis=-1).astype(np.float32)
	flow_neutral = np.repeat(np.expand_dims(flow_neutral, axis=0), MB, axis=0)
	flow_neutral = tr.from_numpy(flow_neutral).to(device)
	flow_torch = flow_neutral - flow

	# Get the flow outside of the regular interval (theoretical mask)
	mask = (((flow_torch < -1) | (flow_torch > 1)).sum(dim=-1) > 0).unsqueeze(dim=0)

	# Apply the mask for _each_ incoming edge, assuming that they corresponding keys are identical, except
	#  t-1 becomes t in the name.
	for k in x["Warp(t-1, t)"]:
		otherK = k.replace("t-1", "t")
		x["Warp(t-1, t)"][k][mask] = x[otherK][mask]

	previous = x.pop("Warp(t-1, t)").values()
	current = x.values()
	y = tr.cat([*current, *previous], dim=0)
	return y.mean(dim=0).unsqueeze(dim=0)
