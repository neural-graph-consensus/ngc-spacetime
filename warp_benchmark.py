import numpy as np
from typing import Iterator
from neural_wrappers.utilities import npGetInfo, RunningMean, getGenerators
from models.warp_flow.warp_flow import npWarpFlow
from models.warp_depth.warp_delta_pose import npWarpDepthPose, computeIntrinsics
from tqdm import trange
from media_processing_lib.image import tryWriteImage

from reader import Reader

def L1(y, t):
	return np.abs(y - t)

def flowFunc(data:np.ndarray, dataPrev:np.ndarray, flow:np.ndarray, dim:str):
	dataPrevWarped = npWarpFlow(dataPrev.transpose(0, 3, 1, 2), flow).transpose(0, 2, 3, 1)
	res = L1(data, dataPrevWarped)
	return res.flatten()

def flowMaskFunc(data:np.ndarray, dataPrev:np.ndarray, flow:np.ndarray, dim:str):
	MB, h, w = flow.shape[0], flow.shape[1], flow.shape[2]
	dataPrevWarped = npWarpFlow(dataPrev.transpose(0, 3, 1, 2), flow).transpose(0, 2, 3, 1)

	flow = (flow - 0.5) * 2
	flow_neutral = np.stack(np.meshgrid(np.linspace(-1, 1, w), np.linspace(-1, 1, h)), axis=-1).astype(np.float32)
	flow_neutral = np.repeat(np.expand_dims(flow_neutral, axis=0), MB, axis=0)
	flow = flow_neutral - flow

	mask = (((flow<-1) | (flow>1)).sum(axis=-1) > 0)
	data = data[~mask]
	dataPrevWarped = dataPrevWarped[~mask]
	res = L1(data, dataPrevWarped)
	return res.flatten()

def dumbFunc(data:np.ndarray, dataPrev:np.ndarray, dim:str):
	res = L1(data, dataPrev)
	return res.flatten()

def depthFunc(data:np.ndarray, dataPrev:np.ndarray, depth:np.ndarray, posePrev:np.ndarray, \
	pose:np.ndarray, K:np.ndarray, dim:str):
	dataPrevWarped = npWarpDepthPose(dataPrev.transpose(0, 3, 1, 2), depth[..., 0] * 300, \
		posePrev, pose, K).transpose(0, 2, 3, 1)
	res = L1(data, dataPrevWarped)
	return res.flatten()

def warp_benchmark(datasetPath:str, batchSize:int):
	Dims = ["rgb", "depth", "wireframe_regression", "semantic_segmentation", "normal", "cameranormal", "halftone"]
	#Dims = ["rgb", "depth", "cameranormal"]
	DimsPrev = ["%s(t-1)" % x for x in Dims]
	DimsExtra = ["optical_flow(t-1, t)", "pose", "pose(t-1)"]
	hyperParameters = {"resolution" : (256, 256), "maxDepthMeters" : 300.0, "positionNormalization" : "none", \
		"orientationRepresentation" : "euler", "orientationNormalization" : "none", \
		"opticalFlowMode" : "xy", "opticalFlowPercentage" : (100, 100), "semanticNumClasses" : 13}

	reader = Reader(datasetPath, dataBuckets={"data" : Dims + DimsPrev + DimsExtra}, \
		desiredShape=(256, 256), numNeighboursAhead=-1, hyperParameters=hyperParameters)
	print(reader.summary())
	generator, numSteps = getGenerators(reader, batchSize, keys=["test"], maxPrefetch=1)

	K = computeIntrinsics(resolution=(256, 256))
	funcKeys = ["dumb", "flow", "depth", "flow_mask"]
	RM = {k : {k2 : RunningMean(0) for k2 in Dims} for k in funcKeys}
	Range = trange(numSteps, desc=". ".join(["%s: 0.000" % x for x in funcKeys]))
	
	for i in Range:
		data, labels = next(generator)
		flow = data["optical_flow(t-1, t)"]
		depth = data["depth"]
		posePrev = data["pose(t-1)"]
		pose = data["pose"]

		# Flow
		for dim, dimPrev in zip(Dims, DimsPrev):
			resDumb = dumbFunc(data[dim], data[dimPrev], dim=dim)
			resFlow = flowFunc(data[dim], data[dimPrev], flow, dim=dim)
			resDepth = depthFunc(data[dim], data[dimPrev], depth, posePrev, pose, K, dim=dim)
			resFlowMask = flowMaskFunc(data[dim], data[dimPrev], flow, dim=dim)

			RM["dumb"][dim].updateBatch(resDumb)
			RM["flow"][dim].updateBatch(resFlow)
			RM["depth"][dim].updateBatch(resDepth)
			RM["flow_mask"][dim].updateBatch(resFlowMask)

		if i % 10 == 0:
			desc = ". ".join(["%s: %2.3f" % (x, RM[x]["rgb"].get() * 255) for x in funcKeys])
			Range.set_description(desc)

	print("Benchmark results:")
	for k in Dims:
		desc = ". ".join(["%s: %2.3f" % (x, RM[x][k].get() * 255) for x in funcKeys])
		print("[%s] %s" % (k, desc))
